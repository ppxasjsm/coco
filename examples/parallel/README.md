A demonstration of how to write parallelised CoCo workflows.

A. Using Dask.

The scripts para_coco.py and para_nococo.py run workflows equivalent to those
in the ../workflows directory, except that replicate min/md jobs are run in
parallel, and (to make the example a little more impressive) the number of
replicates is increased from 8 to 80.

B. Using MPI.
The script mpi_coco.py shows how you can use the mpipool library (github.com/adrn/mpipool) as an alternatie to Dask.

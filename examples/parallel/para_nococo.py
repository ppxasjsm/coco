from coco.wrappers import  amber, pycoco
import os
import dask.bag as db

# set number of cycles, and number of replicates
maxcycles = 10
nreps = 80

# Put all intermediate files in a subdirectory, to keep things uncluttered:
defdir = 'cocofiles'

# create a list that will hold all the trajectory file names
mdtrajectories = []

# define some defaults for the MD and CoCo analysis

amber.DEFAULTS['-p'] = 'penta.top'
amber.DEFAULTS['-c'] = 'penta.crd'
amber.DEFAULTS['-x'] = '{}.nc'

pycoco.DEFAULTS['-o'] = '{}.rst'
pycoco.DEFAULTS['-f'] = 'rst'
pycoco.DEFAULTS['-t'] = 'penta.pdb'
pycoco.DEFAULTS['-n'] = nreps
pycoco.DEFAULTS['--dims'] = 3
pycoco.DEFAULTS['--grid'] = 30

#
# Main loop begins here:
#
mdresults = []
for cycle in range(maxcycles):
    oldmdresults = mdresults
    mdinps = []
    for rep in range(nreps):

        # Set the parameters for the amber job:
        mdinp = amber.new_inputs(defdir=defdir)
        if cycle == 0:
            mdinp['-c'] = 'penta.crd'
        else:
            # The start coordinates for this cycle are the restart coordinates
            # from the last:
            mdinp['-c'] = oldmdresults[rep]['-r']
        mdinp['-i'] = 'md.in'
        # Override the default name for the trajectory file with something more
        # informative:
        mdinp['-x'] = '{0}/cycle{1:02d}rep{2:02d}.nc'.format(defdir,cycle, rep)

        mdinps.append(mdinp)

    # Now run the MD:
    print 'Cycle {}: running md step...'.format(cycle)

    b = db.from_sequence(mdinps).map(amber.run)
    mdresults = b.compute()

    # Stash the results:
    for mdres in mdresults:
        mdtrajectories.append(mdres['-x'])
#
# final CoCo run...
#
print 'Running CoCo...'
        
coinp = pycoco.new_inputs(defdir=defdir)
coinp['-i'] = mdtrajectories

cocores = pycoco.run(coinp)

print 'final results in noCoCo_final.log'
os.rename(cocores['-l'], 'noCoCo_final.log')
